using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.IO;

namespace Ping
{
    class Program
    {
        static void Main(string[] args)
        {
            infoFunc();
            Console.WriteLine("OS: по умолчанию выбрана Windows!");
            Console.WriteLine("Введите адрес хоста: ");
            string address = Console.ReadLine();
            Console.WriteLine("Введите кол-во итераций. Default value is 2");
            int count = int.Parse(Console.ReadLine());
            pingFunc(address, count);

        }
        
        static void infoFunc()
        {
            Console.WriteLine("Утилита <<Ping>>");
            Console.WriteLine("Версия 1.0 - Прототип");
            Console.WriteLine("Авторы: ");
        }

        static void pingFunc(string Address, int ping_count)
        {
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            PingReply reply = ping.Send(Address);
            if(reply.Status == IPStatus.Success)
            {
                Console.WriteLine("Start log....");
                for (int i = 0; i < ping_count; i++)
                {                    
                    Console.WriteLine("Ответ от " + reply.Address.ToString() + ":" + "Число байт=" + reply.Buffer.Length + " Время=" + reply.RoundtripTime + "мс" + " TTL=" + reply.Options.Ttl);
                }
                Console.WriteLine("End log!");
            }
            else
            {
                Console.WriteLine("Ping FAILED!");
            }
        }
    }
}
